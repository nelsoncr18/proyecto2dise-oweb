const usuarioskey = [];

/**
 * Funcion que recorre el localStorage y muestra los usuarios registrados
 */

function obtenerUser() {
    let listRides = [];
    let tableData = JSON.parse(localStorage.getItem('Usuario'));
    let usu = sessionStorage.getItem('UsuarioActivo');
    tableData.forEach(ride => {
        if (usu == ride.user) {
            listRides.push(ride);
        }
    });
    console.log(listRides);
    return listRides;
}

/**
 * Funcion que inserta datos al localStorage
 */

function insertUser() {
	var nombre = document.getElementById('nombre').value;
	var lastname = document.getElementById('lastname').value;
	var phone = document.getElementById('phone').value;
	var nickname = document.getElementById('nickname').value;
	var password = document.getElementById('password').value;
	var repeatpassword = document.getElementById('repeatpassword').vavar
	var user = {
		nombre,
		lastname,
		phone,
		nickname,
		password,
		repeatpassword
	};
	if((nombre != "") && (lastname != "") && (phone != "") && (nickname != "")){
		window.alert('Usuario Registrado');
		usuarioskey.push(user);
		insertToTable('Usuario', user);
	}else{
		window.alert("Campos en blanco por favor digite algo");
	}
}

/**
 * Funcion que limpia los labels
 */

function clearFields() {
	document.getElementById('nombre').value = '';
	document.getElementById('lastname').value = '';
	document.getElementById('phone').value = '';
	document.getElementById('nickname').value = '';
	document.getElementById('password').value = '';
	document.getElementById('repeatpassword').value = '';
}

function TablaRegistro(tableName, tableData) {
	let table = jQuery(`#${tableName}`);

	let rows = "";
	tableData.forEach((user, index) => {
		let row = `<tr><td>Nombre de usuario:</td><td>${user.nombre}</td>`;
		rows += row + '</tr>';
	});
	table.html(rows);

}


//obtenerListaUsuarios();

/**
 * Funcion que obtiene los datos en el localStorage
 */

function obtenerListaUsuarios(){

    if(localStorage.getItem('nombre')){
        let nombre = localStorage.getItem('nombre');
        console.log(nombre);
    }else{
        console.log("No se encuentra nada");
    }
}

/**
 * Funcion que valida si el usuario esta registrado
 * @param {Dato del nombre} pnombre 
 * @param {Contraseña} pcontraseña 
 */

function validar(pnombre,pcontraseña){
    let logged = false;
    let usuarioObtenido = JSON.parse(localStorage.getItem('Usuario'));
    console.log(usuarioObtenido);
    usuarioObtenido.forEach(user => {
                    console.log('entro');
                    if((pnombre == user.nombre) && (pcontraseña == user.password) && (pcontraseña =! "") && (pnombre =! "")){
						console.log('logged in');
						logged = true;
						return logged;
					}
				});
				if(!logged){
					console.log("Usuario o contraseña erroneas");
					window.alert("Usuario o contraseña erroneas");
				}else{
					sessionStorage.setItem('UsuarioActivo', pnombre);
					window.open('infoRides.html');
					window.close('Login.html');
				}
			
}

/**
 * Funcion que llama a diferentes eventos para que realicen procedimientos
 */

function bindEvents() { 
	jQuery('#add-usuario-button').bind('click', function (element) {
		insertUser();
	});
	
	jQuery('#btnlogin').bind('click',funcionlogin);

}

/**
 * Funcion que llama la funcion del login
 */

function funcionlogin(){
	var pUserName = '';
	var pPassword = '';

	pUserName = document.getElementById('nombre').value;
	pPassword = document.getElementById('password').value;

	validar(pUserName,pPassword);
}

bindEvents();
