/**
 * Guarda una llave en especifico en el localStorage
 * @param {*} key the key to store
 * @param {*} value the value associated to the key to be stored
 */
function saveToLocalStorage(key, value) {
    localStorage.setItem(key, JSON.stringify(value));
    return true;
  }

  /**
   * Guarda datos en una tabla en el localStorage
   * @param {Lista que se va a guardar en el localStorage} list 
   */
  function guardarUsuario(list){
    localStorage.setItem('Usuario', JSON.stringify(list));
  }

  /**
   * Funcion que inserta una lista en el localStorage
   * @param {Nombre de la tabla en el que se va a guardar en el localStorage} tableName 
   * @param {La lista a guardar en el localStorage} object 
   */

  function insertToTable(tableName, object){
    let tableData = JSON.parse(localStorage.getItem(tableName));

    if(!tableData){
      tableData = [];
    }
    tableData.push(object);
    localStorage.setItem(tableName, JSON.stringify(tableData));
    console.log(tableData);
    return tableData;
  }

  /**
   * Funcion para eliminar datos de la tabla en el localStorage
   * @param {Nombre de la tabla en el que se va a eliminar en el localStorage} tableName 
   * @param {La lista a guardar en el localStorage} objectId 
   */

  function deleteFromTable(tableName,objectId){
    let tableData = JSON.parse(localStorage.getItem(tableName));
    let usu = sessionStorage.getItem('UsuarioActivo');

    if(!tableData){
      return false;
    }

    let newTableData = [];
    tableData.forEach((element) => {
      if((element.id != objectId) && (usu == element.user)){
        newTableData.push(element);
      }
    });
      localStorage.setItem(tableName, JSON.stringify(newTableData));
      return newTableData;
  }

  /**
   * Funcion que muestra lo que hay en el localStorage
   * @param {Nombre de la tabla en el localStorage} tableName 
   */

  function getTableData(tableName){
    let tableData = JSON.parse(localStorage.getItem(tableName));
    if(!tableData){
      tableData = [];
    }
    return tableData;
  }
