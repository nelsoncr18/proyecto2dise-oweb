/**
 * Funcion que recorre el la seccion de los checks con un for y retorna una lista con los dias de la semana 
 * 
 */
function obtenerdias() {
    let listaDias = [];
    let checkBoxs = document.getElementsByClassName('checks');
    for (let i = 0; i < 7; i++) {
        if (checkBoxs[i].checked == true) {
            listaDias.push(checkBoxs[i].value);
        }
    }
    return listaDias;
}

/**
 * Funcion que recorre el localStorage, guarda en una lista y despues retorna la lista
 */

function obtenerRides() {
    let listRides = [];
    let tableData = JSON.parse(localStorage.getItem('rides_table'));
    let usu = sessionStorage.getItem('UsuarioActivo');
    tableData.forEach(ride => {
        if (usu == ride.user) {
            listRides.push(ride);
        }
    });
    console.log(listRides);
    return listRides;
}

/**
 * 
 * @param {dia que obtiene como parametro para asi guardar en el localstorage los dias de la semana} dia 
 */

function AgregarRide(dia) {
    let nombreRides = document.getElementById('Nombre_rides').value;
    let Ride_inicio = document.getElementById('Ride_inicio').value;
    let Ride_fin = document.getElementById('Ride_fin').value;
    let descripcion = $('#descripcion').val();
    let hora_salida = document.getElementById('hora_salida').value;
    let hora_llegada = document.getElementById('hora_llegada').value;
    let dias = dia;
    let user = sessionStorage.getItem('UsuarioActivo');
    let currentKey = localStorage.getItem('id');
    if(!currentKey){
        localStorage.setItem('id', 1);
        currentKey = 1;
    }else{
        currentKey = parseInt(currentKey) + 1;
		localStorage.setItem('id', currentKey);
    }

    let ride = {
        nombreRides,
        Ride_inicio,
        Ride_fin,
        descripcion,
        hora_salida,
        hora_llegada,
        dias,
        user,
        id: currentKey
    };

    if ((nombreRides != "") && (Ride_inicio != "") && (Ride_fin != "") && (descripcion != "")) {
        
        let rides = insertToTable('rides_table', ride);

    } else {
        window.alert("Campos en blanco porfavor dijite algo");
    }

    Tabla('rides_table', ride);
}

/**
 * Funcion para que llame a la funcion que genera la tabla de los rides
 * @param {Parametro que obtiene el nombre de la tabla que esta en localStorage} tableName 
 * @param {La lista que recibe por parametro para que carge la tabla} list 
 */

function CargaTabla(tableName, list) {
    TablaInicio(tableName, list);
}

/**
 * Funcion que crea una tabla html y muestra el contenido de localStorage
 * @param {Parametro que obtiene el nombre de la tabla que esta en localStorage} tableName 
 * @param {La lista que recibe por parametro para que carge la tabla} tableData 
 */

function Tabla(tableName, tableData) {
    let table = jQuery(`#${tableName}`);

    let rows = "";
    tableData.forEach((ride, index) => {
        let row = `<tr><td>${ride.nombreRides}</td><td>${ride.Ride_inicio}</td><td>${ride.Ride_fin}</td>
		<td>${ride.descripcion}</td><td>${ride.hora_salida}</td><td>${ride.hora_llegada}</td><td>${ride.dias}</td>`;
        row += `<td> <a onclick="editEntity(this)" data-id="${ride.id}" data-entity="${tableName}" class="EditRides.html" data-toggle="modal" data-target="#emod1al">Editar</a>  |  <a onclick="deleteEntity(this);" data-id="${ride.id}" data-entity="${tableName}" class="btn btn-primary link delete">Delete</a>  </td>`;
        rows += row + '</tr>';
    });
    table.html(rows);
}

/**
 * Funcion que crea una tabla html y muestra el contenido de localStorage
 * @param {Parametro que obtiene el nombre de la tabla que esta en localStorage} tableName 
 * @param {La lista que recibe por parametro para que carge la tabla} tableData 
 */

function TablaInicio(tableName, tableData) {
	let table = jQuery(`#${tableName}`);

	let rows = "";
	tableData.forEach((rides, index) => {
		let row = `<tr><td>${rides.nombreRides}</td><td>${rides.Ride_inicio}</td><td>${rides.Ride_fin}</td>
		<td>${rides.descripcion}</td><td>${rides.hora_salida}</td><td>${rides.hora_llegada}</td><td>${rides.dias}</td>`;
		row += `<td> <a onclick="DatosUsuarios(this);" data-id="${rides.id}" data-entity="${tableName}" class="btn btn-primary" data-toggle="modal" data-target="#EditRides.html"> Ver detalles </a>  </td>`;
		rows += row + '</tr>';
	});
	table.html(rows);

}

/**
 * 
 * @param {Funcion que edita los datos del localStorage} dias 
 */

function editar(dias){
	const nombreRides = document.getElementById('ENombre_rides').value;
	const Ride_inicio = document.getElementById('ERide_inicio').value;
	const Ride_fin = document.getElementById('ERide_fin').value;
	const descripcion = document.getElementById('edescripcion').value;
	const hora_salida = document.getElementById('ehora_salida').value;
	const hora_llegada = document.getElementById('ehora_llegada').value;
	const id = document.getElementById('ide').value;

	// create the book object
	let ride = {
        nombreRides,
        Ride_inicio,
        Ride_fin,
        descripcion,
        hora_salida,
        hora_llegada,
        dias,
        id
    };

	let rides = JSON.parse(localStorage.getItem('rides_table'));
    let results = rides.filter(ride => ride.id != id);
	results.push(ride);
    localStorage.setItem('rides_table', JSON.stringify(results));
    
    Tabla('rides_table', results);
    limpiarcamposEditar();
}

/**
 * Funcion que utilza llama procedimiento para realizar el eliminar
 * @param {Parametro como elemento} element 
 */

function deleteEntity(element){
    const dataObj = jQuery(element).data();
    const newEntities = deleteFromTable(dataObj.entity, dataObj.id);
    Tabla(dataObj.entity, newEntities);
}

function editEntity(element) {
	const dataObj = jQuery(element).data();
    console.log(dataObj);
	let rides = JSON.parse(localStorage.getItem('rides_table'));
	let RideEncontrado;
	rides.forEach(function (ride) {
		if (ride.id == dataObj.id) {
			RideEncontrado = ride;
			return;
		}
	});

	document.getElementById('ENombre_rides').value = RideEncontrado.nombreRides;
	document.getElementById('ERide_inicio').value = RideEncontrado.Ride_inicio;
	document.getElementById('ERide_fin').value = RideEncontrado.Ride_fin; 
	document.getElementById('edescripcion').value = RideEncontrado.descripcion;  

}

/**
 * Funcion que limipia los campos 
 */

function limpiarcamposEditar() {
	 document.getElementById('ENombre_rides').value;
	 document.getElementById('ERide_inicio').value;
	 document.getElementById('ERide_fin').value;
	 document.getElementById('edescripcion').value;
	 document.getElementById('ehora_salida').value;
	 document.getElementById('ehora_llegada').value;
}

function loadRide(object){
    let list = getTableData('rides_table');
    for(var i = 0; i < list.length; i++){
        if(object == list[i].id){
            jQuery('#ENombre_rides').val(list[i].nombreRides);
            jQuery('#ERide_inicio').val(list[i].Ride_inicio);
            jQuery('#ERide_fin').val(list[i].Ride_fin);
            jQuery('#edescripcion').val(list[i].descripcion);
            jQuery('#ehora_salida').val(list[i].hora_salida);
            jQuery('#ehora_llegada').val(list[i].hora_llegada);
        }
    }
    sessionStorage.setItem('idCarerra', object);
}

/**
 * Funcion que filtra y recorre el localStorage para 
 */

function buscarRideGlobal(){
    let nuevaTabla = [];
    let tableData = JSON.parse(localStorage.getItem('rides_table'));
    let salida = document.getElementById('OSalida').value;
    let llegada = document.getElementById('OLlegada').value;
    tableData.forEach(ride => {
        if((salida == ride.Ride_inicio) && (llegada == ride.Ride_fin)){
            nuevaTabla.push(ride);
        }
    });
    console.log(nuevaTabla);
    CargaTabla('rides_table',nuevaTabla);
}

/**
 * Funcion que llama a otra funcion para que se muestre en el localstorage
 * @param {Parametro como elemento} element 
 */

function botonBuscar(element){
    buscarRideGlobal();
}

function DatosUsuarios(element) {
	let object = jQuery(element).data();
	cBDatos(object.id);
	console.log(object);
}

/**
 * Funcion que llama a diferentes eventos para que realicen procedimientos
 */

function bindEvents() {
    jQuery('#btnGuardar').bind('click', (element) => {
        AgregarRide(obtenerdias());
    });

    jQuery('#btneditar').bind('click', (element) => {
        editar();
    });

    jQuery('#btnBuscar').bind('click', botonBuscar);
}

bindEvents();